#include <iostream>
#include <vector>
#include <stdio.h>
#include <string.h>
#include <fstream>
#include <errno.h>
#include <stdlib.h>
#define WORD_SIZE 32
#define HEXA_SIZE 4
#define COUNT 1

using namespace std;

//Stores the data from the dataset into attributes
typedef class dataset
{
public:
    string Country_Name, Country_Code, Indicator_Name, Indicator_Code;
    string yr1, yr2, yr3, yr4, yr5;
    class dataset* next;
}dataset;

//Stores the unique value of column to be indexed and the bit array
typedef class indexL
{
public:
    	string uniqueRec;
        vector<int> indexVal;
    	class indexL* next;
}indexL;

//Stores the word and HexWord
typedef class inputSequence
{
public:
    	string uniqueRec;
        vector<int> word;
        vector<char> hexWord;
    	class inputSequence* next;
}inputSequence;

//Pointer declarations and initializations
dataset *rear=NULL, *head=NULL, *temp=NULL;
indexL *iRear=NULL, *iHead=NULL, *iTemp=NULL;
inputSequence *sRear=NULL, *sHead=NULL, *sTemp=NULL;
char *hex_dig="0123456789ABCDEF";
char hexChar;
int hexVal;
string code;

void generateTable(vector<string> temparr)
{
    printf("\n Entered the function....\n");
	int inc=0;
    temp = new dataset;
//Updates the values in the list for the new tuples
	head=temp;
	while(1)
	{
            temp->Country_Name=temparr[inc];
		    temp->Country_Code=temparr[inc+1];
		    temp->Indicator_Name=temparr[inc+2];
		    temp->Indicator_Code=temparr[inc+3];
		    temp->yr1=temparr[inc+4];
		    temp->yr2=temparr[inc+5];
		    temp->yr3=temparr[inc+6];
		    temp->yr4=temparr[inc+7];
		    temp->yr5=temparr[inc+8];
            inc=inc+10;
            rear=temp;
            if(temparr[inc+9]=="TERMINATE")
            break;
            temp = new dataset;
            rear->next=temp;
    }
    temp->next=NULL;
    printf("\n Table generated....\n");

//Create the array of unique values to create the bitmap index
	iTemp = new indexL;
	iTemp->uniqueRec="Empty";
	iHead=iTemp;
	temp=head;
	while(temp!=NULL)
	{
	    do
        {
        if(temp->Country_Code!=iTemp->uniqueRec)
			{
			    if(iTemp->uniqueRec=="Empty")
			    {
                //cout<<"\n Visited";
                iTemp->uniqueRec = temp->Country_Code;
                iRear=iTemp;
                iTemp = new indexL;
                iTemp->uniqueRec="Empty";
                iRear->next=iTemp;
                iTemp->next=NULL;
                goto done;
			    }
			    iTemp=iTemp->next;
            }
        else
			goto done;
        }while(1);
        done:
            temp=temp->next;
            iTemp=iHead;
	}

cout<<"\n Unique list generated....\n";
}

void implementBitmapIndex()
{
    iTemp=iHead;
    temp=head;
	while(iTemp!=NULL)
	{
		while(temp!=NULL)
		{
			if(iTemp->uniqueRec==temp->Country_Code)
			iTemp->indexVal.push_back(1);
			else iTemp->indexVal.push_back(0);
			temp=temp->next;
		}
	temp=head;
	iTemp=iTemp->next;
	}

iTemp=iHead;
cout<<"\n Bitmap Index generated....\n";
}

void implementWAH()
{
iTemp=iHead;
sTemp = new inputSequence;
sHead=sTemp;
int inc=0, add=0;
while(iTemp!=NULL)
{
    inc=0;
    while(inc<iTemp->indexVal.size())
    {
        add=0;
        sTemp->word.push_back(0);
        while(add<WORD_SIZE-1 && inc<iTemp->indexVal.size())
        {
            sTemp->word.push_back(iTemp->indexVal[inc]);
            inc=inc+1;
            add=add+1;
        }
        sRear=sTemp;
        sTemp= new inputSequence;
        sRear->next=sTemp;
        sTemp->next=NULL;
    }
    iTemp=iTemp->next;
}
cout<<"\nWords generated....";

int flag=1;
sTemp=sHead;
while(sTemp->next!=NULL)
{
    flag=1;
    inc=1;
    while(inc<WORD_SIZE-1)
    {
        if(sTemp->word[inc]!=sTemp->word[inc+1])
        {
            flag=0;
            goto done;
        }
        inc=inc+1;
    }
    done:
    if(flag && sTemp->word[1]==1)
        sTemp->word[0]=1;
    if(flag && sTemp->word[1]==1)
        sTemp->word[0]=1;
//Creation of HexaWords
    inc=0;
    if(sTemp->word[0]==1)
    {
    cout<<"\n hexChar:: ";
    for(int i=0;i<sTemp->word.size();i=i+HEXA_SIZE)
        {
            hexVal= (8*sTemp->word[i]) + (4*sTemp->word[i+1]) + (2*sTemp->word[i+2]) + (sTemp->word[i+3]);
            //cout<<"hexVal:"<<hexVal;
            hexChar=hex_dig[hexVal];
            cout<<hexChar;
            sTemp->hexWord.push_back(hexChar);
        }
    }
    sTemp=sTemp->next;
}
}

void selectOperation(string code)
{
int cnt=0;
sTemp=sHead;
while(sTemp!=NULL)
{
    for(int i=0;i<sTemp->hexWord.size();i++)
        cnt=cnt+4;
    for(int i=0;i<sTemp->hexWord.size();i++)
        cnt=(COUNT+1)*31;
sTemp=sTemp->next;
}
cout<<"\nSelect Operation Completed....";
cout<<endl<<"\nThe number of records that are to be returned for the query is "<<cnt;
}

int main()
{
cout << "Bitmap Indexing !!!" << endl;
cout<<"Enter Country Code:  ";
cin>>code;
//Initializing the linklist values to new datasets
//Reading the input file for the data. Data is read into temparr vector and then used to create the data table using link list
cout<<"\nReading the data file...";
       vector<string> temparr;
       int size;
       char line[2000];
       char* temp;
       FILE* inputFile;
inputFile=fopen("Genderstats_Data.csv", "r");
if(!inputFile== NULL)
{
    cout<<"File Opened";
    while(fgets(line, 2000, inputFile)!=NULL)
    {
        temp=strtok(line,",");
        while(temp!=NULL)
        {
           temparr.push_back(temp);
           temp=strtok(NULL,",");
           size=size+1;
        }
    }
temparr.push_back("TERMINATE");
}
else
{
    cout<<"\nError opening the file";
    exit(0);
}

//Calling generateTable function to create the list from the input.
generateTable(temparr);

//implementBitmapIndex function is called to implement BitmapIndex
implementBitmapIndex();

//implementWAH function is called to implement the WAH compression
implementWAH();

//selectOperation function executes the select operation
selectOperation(code);
   return 0;
}

