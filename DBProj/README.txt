/*
Advanced Database Systems Project, Spring 2014
Created by Yamini J & Gaurish U N
*/
The project name is Bitmap Indexes and compression techniques using new strategy. The goal is to implement bitmap index on a huge dataset. Then compress the data using WAH compression technique. We do select operation on the compressed data to retrieved data from the database based on the query. As bitmap indexing is run-length encoding, its an advantage if all the similar bits are together. This makes compression efficient and thus enhancing performance and reduces the storage space. We have implemented the new strategy proposed by researchers which asks for sorting the data before indexing. The data in the column to be indexed is sorted in ascending order and hen the indexing is started. This approach is believed to improve performance.
The readme file has three section where we explain the code in detail. Section 1 explains the datasets, variables and functions used in the coding. Their importance, relation and order of execution. Section 2 covers the execution steps. Section3 covers the expected output, derived output and explanation on the data.

Section 1:
Following are the variable and structs used with their importance and definition.
1. class dataset - Used to store the data read from the csv file. Its members store 9 variables all of type string.
2. class indexL - Used to stored the unique values from the column to be indexed. Its members store the unique value and the bit array to represent respective 	existence in the tuples. The size of bit array is equal to the number of tuples.
3. class inputSequence - Used to store the words. The bit array is split into 31-bit words. The total size of the word is 32-bit but initial bit is left empty to be prefixed with fill bit. If all the 31 bits in the word are 1. the fill bit is 1, similar for 0. The literal words are prefixed with -1.

Following are the functions used in the execution of code.
1. generateTable(vector<string>, size) - Used to read data from the temp array which holds each tuple of data read from the file. The data is splitt into respective variables and saved as a list to form a table.
2. implementBitmapIndex() - Used to implement bitmap index on the table generated. The column on which we do indexing is "CountryCode". The unique codes are saved as a list of values. Each set further contains the bit array.
3. implementWAH() - Used to implement WAH compression technique on the bit array. The compression technique splits the bit array into multiple 32-bit words. Each word is converted in HexaDecimal value, and the compressed further into a set of 15 words with multiple such sets based on the total size of the array.
4. selectOperation - Used to select tuples based on the the input query. When queries for search of a CountryCode, the query returns the total number of existing tuples with the matching Country Code.

Section 2:
The dataset and code are saved in same directory. Once compiled, the code needs "Country Code" input. It reads dataset from the file whose address is pre-defined inside the main  function of the code. The code does the required execution in the sequence and gives the output. We are using code block to execute the code, It should be executable using g++.

Section 3:
 Bitmap Indexing !!!
Enter Country Code:  Paraguay

Reading the data file...File Opened
 Entered the function....

 Table generated....

 Unique list generated....

 Bitmap Index generated....

Words generated....

 hexChar:: FFFFFFFF
 
 Select Operation Completed....
 
The number of records that are to be returned for the query is 62  